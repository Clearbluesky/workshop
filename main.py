import pandas as pd
from sklearn import model_selection
from apyori import apriori
import argparse

def arg_pass():
    parser = argparse.ArgumentParser()
    parser.add_argument('--data',type = str, help='csv file as a dataset')
    return parser

def load_data(filename):
    data = pd.read_csv(filename)
    return data

def preprocess(data):
    item_list = data.unstack().dropna().unique()
    train, test = model_selection.train_test_split(data,test_size =.10)
    train = train.T.apply(lambda x: x.dropna().tolist()).tolist()
    test = test.T.apply(lambda x: x.dropna().tolist()).tolist()
    return train,test

def model(train_data,minsupport = 0.0045,min_confidence=0.2,min_lift=3,min_length=2):
    results = list(apriori(train_data,minsupport = minsupport,min_confidence=min_confidence,min_lift=min_lift,min_length=min_length))
    return results

def virtualize(results):
    for rule in results:
        pair = rule[0]
        components = [i for i in pair]
        print(pair)
        print('Rule:',components[0],'->',components[1])
        print('Support:',rule[1])
        print('Confidence:',rule[2][0][2])
        print('Lift:',rule[2][0][3])
        print('==================================================')

if __name__ == '__main__':
    parser = arg_pass()
    args = parser.parse_args()
    print (args.data)
    data = load_data(args.data)
    train,test = preprocess(data)
    results = model(train)
    virtualize(results)